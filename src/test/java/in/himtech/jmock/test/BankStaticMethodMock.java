package in.himtech.jmock.test;

import junit.framework.Assert;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.testng.annotations.Test;

/**
 * This class will show demo of mocking of static method in
 * behaviour(Expectation) way.
 * 
 * @author himanshu
 *
 */
public class BankStaticMethodMock {

	@Mocked
	DBManager dbManager;

	/**
	 * Behaviour way to mock satic method.
	 */
	@Test
	public void testDBName() {

		new NonStrictExpectations() {
			{
				DBManager.getDatabaseName();
				returns("MYSQL");
			}
		};
		Bank bank = new Bank();
		String str = bank.getDBData();
		Assert.assertEquals(str, "Ghanta Database");
	}
}
