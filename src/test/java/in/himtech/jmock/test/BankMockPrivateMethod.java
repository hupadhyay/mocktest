package in.himtech.jmock.test;

import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;
import mockit.NonStrictExpectations;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * This class will show demo of mocking of private method in both way
 * behaviour(Expectation) and state(MockUp)
 * 
 * @author himanshu
 *
 */
public class BankMockPrivateMethod {

	/**
	 * This method show demo of mocking of private method of
	 * <class>DBManager</class> using Expectation class for behaviour mocking
	 */
	@Test
	public void testProcessConnection() {
		Bank bank = new Bank();
		DBManager dbManager = bank.getDbManager();
		new NonStrictExpectations(dbManager) {
			{
				Deencapsulation.invoke(dbManager, "prepareConnectionString");
				returns("Anushka");
			}
		};

		String name = bank.processConnection();
		Assert.assertEquals(name, "Anushka");
	}

	/**
	 * This method show demo of mocking of private method of
	 * <class>DBManager</class> using MockUp class for statebased mocking
	 */
	@Test
	public void testProcessConnectionMockUp() {
		new MockUp<DBManager>() {
			@Mock
			String prepareConnectionString() {
				return "Anushka";
			}
		};
		Bank bank = new Bank();
		String name = bank.processConnection();
		Assert.assertEquals(name, "Anushka");
	}
}
