package in.himtech.jmock.test;

import mockit.Deencapsulation;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class BankTest {

//	@Mocked
//	DBManager dbManager;

	/**
	 * This test case use Expectations api to mock "retrieveAccountHolderName"
	 * method of "dbManager". In Expectations class the mocked method can use
	 * only once.
	 */
	@Test
	public void testProcessAccount() {
		Bank bank = new Bank();
		DBManager dbManager = bank.getDbManager();
		new Expectations(dbManager) {

			{
				dbManager.retrieveAccountHolderName(anyInt);
				returns("Hrishik");
			}
		};

		String name = bank.processAccount(10);
		Assert.assertEquals(name, "Hrishik");
	}

	/**
	 * This test case use NonStrictExpectations api to mock
	 * "retrieveAccountHolderName" method of "dbManager". It doesn't put any
	 * restriction on number of times the mocked method can use.
	 */
	@Test
	public void testProcessAccount1() {
		Bank bank = new Bank();
		DBManager dbManager = bank.getDbManager();
		new NonStrictExpectations(dbManager) {
			{
				dbManager.retrieveAccountHolderName(anyInt);
				returns("Hrishik");
			}
		};

		String name = bank.processAccount(10);
		Assert.assertEquals(name, "Hrishik");
	}
	
	

}
