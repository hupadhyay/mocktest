package in.himtech.jmock.test;

import org.junit.Assert;
import org.testng.annotations.Test;

import mockit.Mock;
import mockit.MockUp;

/**
 * This class demonstrate Constructor(default and parameterized) and static block mocking.
 * We mock the constructor by using a special mock method $init.
 * To mock static block use mock method $clinit. In this case, we mock static block to get 
 * counter value 500 in place of 100.
 */
@Test
public class PersonTest {

	/**
	 * Mocking of static block.
	 */
	@Test(enabled=false)
	public void testStaticBlock() {
		new MockUp<Person>() {
			@Mock
			public void $clinit(){
				Person.updateCounter(500);
			}
		};
		
		Assert.assertEquals("The number of person is 500.", Person.getCounter(), 500);;
	}
	
	/**
	 * Mocking of default constructor.
	 */
	@Test
	public void testGetPerson() {
		new MockUp<Person>() {
			@Mock
			public void $init(){
				
			}
		};
		
		Person person = new Person();
		Assert.assertNull(person.getName());
	}
	
	/**
	 * Mocking of parameterized constructor.
	 */
	@Test
	public void testGetPersonWithParamCons() {
		new MockUp<Person>() {
			@Mock
			public void $init(String str){
				
			}
		};
		
		Person person = new Person("Hello");
		Assert.assertNull(person.getName());
	}

}
