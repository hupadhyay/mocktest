package in.himtech.jmock.test;

import junit.framework.Assert;
import mockit.Mock;
import mockit.MockUp;

import org.testng.annotations.Test;

/**
 * This class will show demo of mocking of static method in state(MockUp) way.
 * 
 * @author himanshu
 *
 */
public class BankStaticMethodMockUp {
	/**
	 * State way to mock satic method.
	 */
	@Test
	public void testDBNameMockUp() {
		new MockUp<DBManager>() {
			@Mock
			public String getDatabaseName() {
				return "MySql";
			}
		};
		Bank bank = new Bank();
		String str = bank.getDBData();
		Assert.assertEquals(str, "Ghanta Database");
	}
}
