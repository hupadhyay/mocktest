package in.himtech.jmock.test;

import org.testng.annotations.Test;

import mockit.NonStrictExpectations;

/**
 * this class give a demo of handling of exception in Expectation block.
 * 
 * @author himanshu
 *
 */
public class BankExceptionTest {

	/**
	 * HERE, there is no need to use @Mocked Bank bank. As in the constructor we pass object of Bank.
	 * This will automatically mocked object.
	 */
	@Test(expectedExceptions = Exception.class)
	public void testWithdrawMoney() throws Exception{
		Bank bank = new Bank();
		new NonStrictExpectations(bank) {
			{
				bank.withdrawMoney(anyInt);
				result = new Exception();
			}
		};
		bank.withdrawMoney(300);

	}
}
