package in.himtech.jmock.test;

import mockit.Mock;
import mockit.MockUp;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class BankTestState {

	/**
	 * This test use MockUp api to do state base testing.
	 */
	@Test
	public void testProcessAccountMockUp() {
		new MockUp<DBManager>() {
			@Mock
			public String retrieveAccountHolderName(int accntNumber) {
				return "Himanshu";
			}
		};
		Bank bank = new Bank();
		String name = bank.processAccount(10);
		Assert.assertEquals(name, "Himanshu");
	}
}
