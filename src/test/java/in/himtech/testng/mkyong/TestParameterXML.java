package in.himtech.testng.mkyong;

import junit.framework.Assert;

import org.testng.annotations.Test;
import org.testng.annotations.Parameters;

public class TestParameterXML {

	@Test
	@Parameters({ "offset", "limit" })
	public void createConnection(int offset, int limit) {
		System.out.println("Offset: " + offset);
		System.out.println("Limit: " + limit);
	}
	
	@Test
	public void makeItFail(){
		Assert.assertTrue(false);
	}
	
	@Test(dependsOnMethods="makeItFail")
	public void mayBeSkipped(){
		Assert.assertTrue(true);
	}
	
}
