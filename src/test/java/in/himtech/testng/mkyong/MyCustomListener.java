package in.himtech.testng.mkyong;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class MyCustomListener extends TestListenerAdapter {

	@Override
	public void onTestFailure(ITestResult tr) {
		System.out.println(tr.getName() + ": this method is failed");
	}
	
	
	@Override
	public void onTestSkipped(ITestResult tr) {
		System.out.println(tr.getName() + ": this method is skipped");
	}
	
	@Override
	public void onTestSuccess(ITestResult tr) {
		System.out.println(tr.getName() + ": this method is successed");
	}
}
