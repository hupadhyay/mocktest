package in.himtech.testng.mkyong;

import org.testng.annotations.Test;

public class DependTest {
	@Test
	public void testFirst() {
		System.out.println("executed First");
		throw new ArithmeticException();
	}
	
	@Test
	public void testThird() {
		System.out.println("executed third");
	}
	

	@Test(dependsOnMethods={"testFirst", "testThird"})
	public void testSecond() {
		System.out.println("executed second");
	}
}
