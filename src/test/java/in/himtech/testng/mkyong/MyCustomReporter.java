package in.himtech.testng.mkyong;

import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.xml.XmlSuite;

public class MyCustomReporter implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> listXmlSuite, List<ISuite> listSuite,
			String outputDirectory) {
		
		for(ISuite suite : listSuite){
			System.out.println("Suite Name:" + suite.getName());
			
			Map<String, ISuiteResult> map =suite.getResults();
			
			for(ISuiteResult isr : map.values()){
				ITestContext testContext = isr.getTestContext();
				
				System.out.println("Number Of All Passed Testcases is: " + testContext.getPassedTests().size());
				System.out.println("Number Of All Failed Testcases is: " + testContext.getFailedTests().size());
				System.out.println("Number Of All Skipped Testcases is: " + testContext.getSkippedTests().size());
			}
		}

	}

}
