package in.himtech.testng.mkyong;

import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

public class TestConfig {

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("After Method");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("Before Class");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("After Class");
	}

	@BeforeGroups("mygroup")
	public void beforeGroup() {
		System.out.println("Before Group");
	}

	@AfterGroups("mygroup")
	public void afterGroup() {
		System.out.println("After Group");
	}
	
	@Test(groups = "mygroup")
	public void test1(){
		System.out.println("Running of Test 1");
	}

	@Test
	public void test2(){
		System.out.println("Running of Test 2");
	}
	
	/** by default enable is true, if you want to ignore test make it false */
	@Test(enabled=false)
	public void ignoredTest(){
		System.out.println("this is ignored test");
	}
	
	@Test(timeOut = 2000) // time in mulliseconds
	public void testThisShouldPass() throws InterruptedException {
		Thread.sleep(1500);
	}
	
	@Test(timeOut = 2000) // time in mulliseconds
	public void testThisShouldfail() throws InterruptedException {
		Thread.sleep(2500);
	}
}
