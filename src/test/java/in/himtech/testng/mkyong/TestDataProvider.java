package in.himtech.testng.mkyong;

import java.lang.reflect.Method;

import junit.framework.Assert;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestDataProvider {

	@DataProvider(name = "intprovider")
	public Object[][] integerProvider() {
		return new Object[][] { { 4, 14 }, { 12, 22 }, { 45, 55 } };
	}

	@DataProvider(name="methProvider")
	public Object[][] dataProvider(Method method) {
		Object[][] obj = null;

		if (method.getName().equals("testMeth1")) {
			obj = new Object[][] { { 34, 36 }, { 56, 58 }, { 90, 92 },
					{ 85, 87 } };
		} else if (method.getName().equals("testMeth2")) {
			obj = new Object[][] { { "Hello", "HelloSir" }, { "GM", "GMSir" },
					{ "Namaste", "NamasteSir" }, { "Welcome", "WelcomeSir" } };
		}
		return obj;
	}

	@Test(dataProvider = "intprovider")
	public void testIntegerValue(int val1, int val2) {
		Assert.assertEquals(val1 + 10, val2);
	}
	
	@Test(dataProvider = "methProvider")
	public void testMeth1(int val1, int val2) {
		Assert.assertEquals(val1 + 2, val2);
	}
	
	@Test(dataProvider = "methProvider")
	public void testMeth2(String val1, String val2) {
		Assert.assertEquals(val1 + "Sir", val2);
	}
}
