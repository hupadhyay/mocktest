package in.himtech.testng.mkyong;

import org.testng.annotations.Test;

public class DBConnectionTest {
	@Test
	public void testConnMySqlDB() {
		System.out.println("test connection to mysql");
	}
	
	@Test
	public void testConnOracleDB() {
		System.out.println("test connection to Oracle");
	}
}
