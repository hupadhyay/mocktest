package in.himtech.testng.parallel;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ParallelClassTest3 {
	
	@BeforeClass
	public void beforeClass(){
		System.out.println("BeforeClass : " + this.getClass().getName());
	}
	
	@Test
	public void testM1(){
		System.out.println("M1 : " + this.getClass().getName());
	}

	@Test
	public void testM2(){
		System.out.println("M2 : " + this.getClass().getName());
	}
	
	@Test
	public void testM3(){
		System.out.println("M3 : " + this.getClass().getName());
	}
	
	@Test
	public void testM4(){
		System.out.println("M4 : " + this.getClass().getName());
	}
	
	@Test
	public void testM5(){
		System.out.println("M5 : " + this.getClass().getName());
	}
	
	@Test
	public void testM6(){
		System.out.println("M6 : "  + this.getClass().getName());
	}
	
	@Test
	public void testM7(){
		System.out.println("M7 :" + this.getClass().getName());
	}
	
	@Test
	public void testM8(){
		System.out.println("M8 : " + this.getClass().getName());
	}
	
	@AfterClass
	public void afterClass(){
		System.out.println("AfterClass: " + this.getClass().getName());
	}
}
