package in.himtech.testng.parallel;

import org.testng.annotations.Test;

/**
 * The priority doesn't have any meaning when you doesn't schedule it for parallel execution.
 * We can schedule a testcase, testclass or testmethod for parallel execution. We can configure 
 * parallel execution in test suite xml file.
 * 
 * @author himanshu
 */
@Test(priority=-1)
public class MyPriorityTest {
	
	@Test
	public void testM1(){
		System.out.println("M1");
	}

	@Test
	public void testM2(){
		System.out.println("M2");
	}
	
	@Test
	public void testM3(){
		System.out.println("M3");
	}
	
	@Test
	public void testM4(){
		System.out.println("M4");
	}
	
	@Test
	public void testM5(){
		System.out.println("M5");
	}
	
	@Test(priority=1)
	public void testM6(){
		System.out.println("M6");
	}
	
	@Test(priority=3)
	public void testM7(){
		System.out.println("M7");
	}
	
	@Test(priority=2)
	public void testM8(){
		System.out.println("M8");
	}
}
