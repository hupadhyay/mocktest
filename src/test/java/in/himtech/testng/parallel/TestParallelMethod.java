package in.himtech.testng.parallel;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * This class will test parallel execution of test cases. 
 * 
 * @author himanshu
 */
@Test(priority=-1)
public class TestParallelMethod {
	
	@BeforeClass
	public void beforeClass(){
		System.out.println("BeforeClass");
	}
	
	@Test
	public void testM1(){
		System.out.println("M1");
	}

	@Test
	public void testM2(){
		System.out.println("M2");
	}
	
	@Test(priority=-1)
	public void testM3(){
		System.out.println("M3");
	}
	
	@Test(priority=1)
	public void testM4(){
		System.out.println("M4");
	}
	
	@Test(priority=3)
	public void testM5(){
		System.out.println("M5");
	}
	
	@Test(priority=3)
	public void testM6(){
		System.out.println("M6");
	}
	
	@Test(priority=0)
	public void testM7(){
		System.out.println("M7");
	}
	
	@Test(priority=0)
	public void testM8(){
		System.out.println("M8");
	}
	
	@AfterClass
	public void afterClass(){
		System.out.println("AfterClass");
	}
}
