package in.himtech.testng.parallel;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * The priority doesn't have any meaning when you doesn't schedule it for parallel execution.
 * We can schedule a testcase, testclass or testmethod for parallel execution. We can configure 
 * parallel execution in test suite xml file.
 * 
 * @author himanshu
 */
@Test
public class ParallelClassTest1 {
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("BeforeTest");
	}
	
	@BeforeClass
	public void beforeClass(){
		System.out.println("BeforeClass : " + this.getClass().getName());
	}
	
	@Test
	public void testM1(){
		System.out.println("M1 : " + this.getClass().getName());
	}

	@Test
	public void testM2(){
		System.out.println("M2 : " + this.getClass().getName());
	}
	
	@Test
	public void testM3(){
		System.out.println("M3 : " + this.getClass().getName());
	}
	
	@Test
	public void testM4(){
		System.out.println("M4 : " + this.getClass().getName());
	}
	
	@Test
	public void testM5(){
		System.out.println("M5 : " + this.getClass().getName());
	}
	
	@Test
	public void testM6(){
		System.out.println("M6 : "  + this.getClass().getName());
	}
	
	@Test
	public void testM7(){
		System.out.println("M7 :" + this.getClass().getName());
	}
	
	@Test
	public void testM8(){
		System.out.println("M8 : " + this.getClass().getName());
	}
	
	@AfterClass
	public void afterClass(){
		System.out.println("AfterClass: " + this.getClass().getName());
	}
	
	@AfterTest
	public void afterTest(){
		System.out.println("BeforeTest");
	}
}
