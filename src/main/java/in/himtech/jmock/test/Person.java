package in.himtech.jmock.test;

/**
 * Hello world!
 *
 */
public class Person 
{
    private String name;
    private static int counter;
    
    public Person() {
		name = "Himanshu";
	}

	public Person(String name) {
		this.name = name;
	}
	
	static{
		updateCounter(100);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static void updateCounter(int i) {
		counter = i;
	}
	
	public static int getCounter(){
		return counter;
	}
}
