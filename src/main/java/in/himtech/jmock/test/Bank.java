package in.himtech.jmock.test;

public class Bank {

	private DBManager dbManager = new DBManager();;
	
	public Bank() {
//		dbManager = new DBManager();
		
	}
	
	public String processAccount(int accntNumber){
		String accntHolderName = dbManager.retrieveAccountHolderName(accntNumber);
		return accntHolderName;
	}
	
	public DBManager getDbManager() {
		return dbManager;
	}
	
	public String processConnection(){
		return dbManager.getConnectionString();
	}
	
	public String getDBData(){
		String str = DBManager.getDatabaseName();
		
		if(str.equals("Oracle")){
			return "Oracle Database";
		} else {
			return "Ghanta Database";
		}
	}
	
	public int withdrawMoney(int amount) throws Exception{
		if (amount <=0){
			throw new Exception("Amount must be greater than 0");
		} else {
			return amount;
		}
	}
}
