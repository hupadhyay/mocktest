package in.himtech.jmock.test;

public class DBManager {

	public String retrieveAccountHolderName(int accntNumber){
		String accntHolder = null;
		// Some DB Operation.
		accntHolder = "Hritika";
		return accntHolder;
	}
	
	public String getConnectionString(){
		return prepareConnectionString();
	}
	
	private String prepareConnectionString(){
		return "Private Method";
	}
	
	
	public static String getDatabaseName(){
		return "Oracle";
	}
}
