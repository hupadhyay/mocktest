# README #

This project contains example of TestNG and JMockit apis. 

### What is this repository for? ###

* TestNG unit test cases.
* JMockit based unit test cases
* Mockito based unit test cases
* PowerMock based unit test cases
* http://abhinandanmk.blogspot.in/2012/06/jmockit-tutoriallearn-it-today-with.html

### What is mocking: ###

Mocking Of Public Method:
There are two way to do it:
1. Behaviour based testing using the Expectation class
2. State based testing using tyhe MockUp apis

In the behaviour based testing technique, we will be creating a Expectations anonymous inner class and 
define the expected behaviour when a method is invoked with particular arguments. 


### Parallel execution of test cases ###

* Method based : methods of single class run parallel.
* Class based : methods of many classes run parallel.
* Test based : methods of many test can run parallel.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact